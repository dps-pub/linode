#!/bin/bash

# Check if MySQL is running
sudo service nginx status > /dev/null 2>&1

# Restart the MySQL service if it's not running.
if [ $? != 0 ]; then
    sudo systemctl restart nginx
fi