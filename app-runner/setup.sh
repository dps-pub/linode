#!/bin/bash

####################################################
## SETUP.SH - PROVISION THE HOST                  ##
####################################################

set -e
clear

export DEBIAN_FRONTEND=noninteractive

####################################################
## Update the host
####################################################

sudo apt-get update
sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages full-upgrade

####################################################
## Unattended Upgrades
####################################################

sudo apt-get install -y unattended-upgrades update-notifier-common
sudo systemctl enable unattended-upgrades
sudo systemctl restart unattended-upgrades

echo 'Unattended-Upgrade::Mail "cto@dps.com.au";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::MailReport  "on-change";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-WithUsers "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-Time "20:00";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades

####################################################
## Install generic tools
####################################################

sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages install pv software-properties-common supervisor

####################################################
## MySQL Client
####################################################

sudo apt-get install -y mysql-client

####################################################
## Redis
####################################################

sudo apt-get install -y redis-server
sudo sed -i 's/^supervised no/supervised systemd/g' /etc/redis/redis.conf
sudo systemctl restart redis

####################################################
## Nginx - Installation
####################################################

sudo apt-get install -y nginx certbot python3-certbot-nginx

sudo chmod 775 /var/www
sudo chown -R dps /var/www
sudo chgrp -R web /var/www
sudo chmod g+s /var/www

sudo sed -i "s/.*# server_tokens off.*/        server_tokens off;/g" /etc/nginx/nginx.conf
sudo systemctl restart nginx.service

echo "* * * * * /home/dps/linode/app-analytics/nginx-restart.sh > /dev/null 2>&1" | crontab -

####################################################
## Nodejs
###################################################

curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs

####################################################
## Font Awesome Pro
###################################################

npm config set "@fortawesome:registry" https://npm.fontawesome.com/
npm config set "//npm.fontawesome.com/:_authToken" 33D79871-BE5F-4E96-ABAE-EF7231039D95

####################################################
## PHP (v8.1)
###################################################

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php8.1-fpm php8.1-imagick php8.1-cli php8.1-xml php8.1-curl \
  php8.1-redis php8.1-dom php8.1-mbstring php8.1-zip php8.1-gd \
  php8.1-mysql php8.1-common php8.1-intl php8.1-opcache php8.1-dev php8.1-bcmath

####################################################
## PHP Composer
###################################################

curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo  php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php

####################################################
## Nginx Virtual Hosts
###################################################

# AWS Monitor - aws-monitor.dps.com.au

mkdir /var/www/aws-monitor.dps.com.au \
  /var/www/aws-monitor.dps.com.au/public \
  /var/www/aws-monitor.dps.com.au/releases \
  /var/www/aws-monitor.dps.com.au/storage \
  /var/www/aws-monitor.dps.com.au/storage/app \
  /var/www/aws-monitor.dps.com.au/storage/app/public \
  /var/www/aws-monitor.dps.com.au/storage/framework \
  /var/www/aws-monitor.dps.com.au/storage/framework/cache \
  /var/www/aws-monitor.dps.com.au/storage/framework/cache/data \
  /var/www/aws-monitor.dps.com.au/storage/framework/sessions \
  /var/www/aws-monitor.dps.com.au/storage/framework/testing \
  /var/www/aws-monitor.dps.com.au/storage/framework/views \
  /var/www/aws-monitor.dps.com.au/storage/logs

chmod -R 775 /var/www/aws-monitor.dps.com.au/storage
sudo cp ~/linode/app-runner/horizon/horizon-aws-monitor.dps.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/app-runner/env/aws-monitor.dps.com.au /var/www/aws-monitor.dps.com.au/.env

sudo cp /home/dps/linode/app-runner/nginx/aws-monitor.dps.com.au.conf /etc/nginx/sites-available/aws-monitor.dps.com.au.conf
sudo ln -s /etc/nginx/sites-available/aws-monitor.dps.com.au.conf /etc/nginx/sites-enabled/aws-monitor.dps.com.au.conf

# DPS Website - dps.com.au

mkdir /var/www/dps.com.au \
  /var/www/dps.com.au/public \
  /var/www/dps.com.au/releases \
  /var/www/dps.com.au/storage \
  /var/www/dps.com.au/storage/app \
  /var/www/dps.com.au/storage/app/public \
  /var/www/dps.com.au/storage/framework \
  /var/www/dps.com.au/storage/framework/cache \
  /var/www/dps.com.au/storage/framework/cache/data \
  /var/www/dps.com.au/storage/framework/sessions \
  /var/www/dps.com.au/storage/framework/testing \
  /var/www/dps.com.au/storage/framework/views \
  /var/www/dps.com.au/storage/logs

chmod -R 775 /var/www/dps.com.au/storage
sudo cp ~/linode/app-runner/horizon/horizon-dps.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/app-runner/env/dps.com.au /var/www/dps.com.au/.env

sudo cp /home/dps/linode/app-runner/nginx/dps.com.au.conf /etc/nginx/sites-available/dps.com.au.conf
sudo ln -s /etc/nginx/sites-available/dps.com.au.conf /etc/nginx/sites-enabled/dps.com.au.conf

# DPS Website (preview) - previewdps.com.au

mkdir /var/www/preview.dps.com.au \
  /var/www/preview.dps.com.au/public \
  /var/www/preview.dps.com.au/releases \
  /var/www/preview.dps.com.au/storage \
  /var/www/preview.dps.com.au/storage/app \
  /var/www/preview.dps.com.au/storage/app/public \
  /var/www/preview.dps.com.au/storage/framework \
  /var/www/preview.dps.com.au/storage/framework/cache \
  /var/www/preview.dps.com.au/storage/framework/cache/data \
  /var/www/preview.dps.com.au/storage/framework/sessions \
  /var/www/preview.dps.com.au/storage/framework/testing \
  /var/www/preview.dps.com.au/storage/framework/views \
  /var/www/preview.dps.com.au/storage/logs

chmod -R 775 /var/www/preview.dps.com.au/storage
sudo cp ~/linode/app-runner/horizon/horizon-preview.dps.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/app-runner/env/preview.dps.com.au /var/www/preview.dps.com.au/.env

sudo cp /home/dps/linode/app-runner/nginx/preview.dps.com.au.conf /etc/nginx/sites-available/preview.dps.com.au.conf
sudo ln -s /etc/nginx/sites-available/preview.dps.com.au.conf /etc/nginx/sites-enabled/preview.dps.com.au.conf

####################################################
## Supervisor (Restart)
###################################################

sudo systemctl restart supervisor

####################################################
## Nginx (Complete and HTTPS Certificates)
###################################################

sudo systemctl restart nginx

sudo certbot \
  --nginx \
  --non-interactive \
  --agree-tos \
  --expand \
  -m cto@dps.com.au \
  -d aws-monitor.dps.com.au \
  -d dpspublishing.com.au \
  -d www.dpspublishing.com.au \
  -d dps.com.au \
  -d www.dps.com.au \
  -d preview.dps.com.au

####################################################
## Iptables - block HTTP/S access
####################################################

#sudo iptables -A INPUT -p tcp -m tcp -m multiport --dports 22,80,443 -j ACCEPT
#sudo iptables -A INPUT -m conntrack -j ACCEPT  --ctstate RELATED,ESTABLISHED
#sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A INPUT -j DROP
#sudo iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A OUTPUT -j DROP
#sudo iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A FORWARD -j DROP

# Add iptables helper methods to bashrc
cat ~/linode/app-runner/bashrc-helpers >> ~/.bashrc

####################################################
## Misc
####################################################

# Set hostname
echo "app-runner-01.dps.com.au" | sudo tee /etc/hostname

####################################################
## Finishing
###################################################

echo "-------------------------------------------"
echo "Please don't forget to reboot this machine"
echo "for all security updates to take hold."
echo "-------------------------------------------"
