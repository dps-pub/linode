#!/bin/bash

####################################################
## SETUP.SH - PROVISION THE HOST                  ##
####################################################

set -e
clear

export DEBIAN_FRONTEND=noninteractive

####################################################
## Update the host
####################################################

sudo apt-get update
sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages full-upgrade

####################################################
## Unattended Upgrades
####################################################

sudo apt-get install -y unattended-upgrades update-notifier-common
sudo systemctl enable unattended-upgrades
sudo systemctl restart unattended-upgrades

echo 'Unattended-Upgrade::Mail "cto@dps.com.au";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::MailReport  "on-change";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-WithUsers "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-Time "20:00";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades

####################################################
## Install generic tools
####################################################

sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages install pv software-properties-common supervisor

####################################################
## MySQL Client
####################################################

sudo apt-get install -y mysql-client

####################################################
## Redis
####################################################

sudo apt-get install -y redis-server
sudo sed -i 's/^supervised no/supervised systemd/g' /etc/redis/redis.conf
sudo systemctl restart redis

####################################################
## Nginx - Installation
####################################################

sudo apt-get install -y nginx certbot python3-certbot-nginx

sudo chmod 775 /var/www
sudo chown -R dps /var/www
sudo chgrp -R web /var/www
sudo chmod g+s /var/www

sudo sed -i "s/.*# server_tokens off.*/        server_tokens off;/g" /etc/nginx/nginx.conf
sudo systemctl restart nginx.service

echo "* * * * * /home/dps/linode/hello-leaders-management/nginx-restart.sh > /dev/null 2>&1" | crontab -

####################################################
## Nodejs
###################################################

curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs

####################################################
## Font Awesome Pro
###################################################

npm config set "@fortawesome:registry" https://npm.fontawesome.com/
npm config set "//npm.fontawesome.com/:_authToken" 33D79871-BE5F-4E96-ABAE-EF7231039D95

####################################################
## PHP (v8.1)
###################################################

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php8.1-fpm php8.1-imagick php8.1-cli php8.1-xml php8.1-curl \
  php8.1-redis php8.1-dom php8.1-mbstring php8.1-zip php8.1-gd \
  php8.1-mysql php8.1-common php8.1-intl php8.1-opcache php8.1-dev php8.1-bcmath

####################################################
## PHP Composer
###################################################

curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo  php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php

####################################################
## Nginx Virtual Hosts
###################################################

# Hello Leaders - admin.helloleaders.com.au

mkdir /var/www/admin.helloleaders.com.au \
  /var/www/admin.helloleaders.com.au/public \
  /var/www/admin.helloleaders.com.au/releases \
  /var/www/admin.helloleaders.com.au/storage \
  /var/www/admin.helloleaders.com.au/storage/app \
  /var/www/admin.helloleaders.com.au/storage/app/public \
  /var/www/admin.helloleaders.com.au/storage/framework \
  /var/www/admin.helloleaders.com.au/storage/framework/cache \
  /var/www/admin.helloleaders.com.au/storage/framework/cache/data \
  /var/www/admin.helloleaders.com.au/storage/framework/sessions \
  /var/www/admin.helloleaders.com.au/storage/framework/testing \
  /var/www/admin.helloleaders.com.au/storage/framework/views \
  /var/www/admin.helloleaders.com.au/storage/logs

chmod -R 775 /var/www/admin.helloleaders.com.au/storage
sudo cp ~/linode/hello-leaders-management/horizon/horizon-admin.helloleaders.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/hello-leaders-management/env/admin.helloleaders.com.au /var/www/admin.helloleaders.com.au/.env

sudo cp /home/dps/linode/hello-leaders-management/nginx/admin.helloleaders.com.au.conf /etc/nginx/sites-available/admin.helloleaders.com.au.conf
sudo ln -s /etc/nginx/sites-available/admin.helloleaders.com.au.conf /etc/nginx/sites-enabled/admin.helloleaders.com.au.conf

# Hello Leaders (Preview) - preview.admin.helloleaders.com.au

mkdir /var/www/preview.admin.helloleaders.com.au \
  /var/www/preview.admin.helloleaders.com.au/public \
  /var/www/preview.admin.helloleaders.com.au/releases \
  /var/www/preview.admin.helloleaders.com.au/storage \
  /var/www/preview.admin.helloleaders.com.au/storage/app \
  /var/www/preview.admin.helloleaders.com.au/storage/app/public \
  /var/www/preview.admin.helloleaders.com.au/storage/framework \
  /var/www/preview.admin.helloleaders.com.au/storage/framework/cache \
  /var/www/preview.admin.helloleaders.com.au/storage/framework/cache/data \
  /var/www/preview.admin.helloleaders.com.au/storage/framework/sessions \
  /var/www/preview.admin.helloleaders.com.au/storage/framework/testing \
  /var/www/preview.admin.helloleaders.com.au/storage/framework/views \
  /var/www/preview.admin.helloleaders.com.au/storage/logs

chmod -R 775 /var/www/preview.admin.helloleaders.com.au/storage
sudo cp ~/linode/hello-leaders-management/horizon/horizon-preview.admin.helloleaders.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/hello-leaders-management/env/preview.admin.helloleaders.com.au /var/www/preview.admin.helloleaders.com.au/.env

sudo cp /home/dps/linode/hello-leaders-management/nginx/preview.admin.helloleaders.com.au.conf /etc/nginx/sites-available/preview.admin.helloleaders.com.au.conf
sudo ln -s /etc/nginx/sites-available/preview.admin.helloleaders.com.au.conf /etc/nginx/sites-enabled/preview.admin.helloleaders.com.au.conf

# Hello Leaders - my.helloleaders.com.au

mkdir /var/www/my.helloleaders.com.au \
  /var/www/my.helloleaders.com.au/public \
  /var/www/my.helloleaders.com.au/releases \
  /var/www/my.helloleaders.com.au/storage \
  /var/www/my.helloleaders.com.au/storage/app \
  /var/www/my.helloleaders.com.au/storage/app/public \
  /var/www/my.helloleaders.com.au/storage/framework \
  /var/www/my.helloleaders.com.au/storage/framework/cache \
  /var/www/my.helloleaders.com.au/storage/framework/cache/data \
  /var/www/my.helloleaders.com.au/storage/framework/sessions \
  /var/www/my.helloleaders.com.au/storage/framework/testing \
  /var/www/my.helloleaders.com.au/storage/framework/views \
  /var/www/my.helloleaders.com.au/storage/logs

chmod -R 775 /var/www/my.helloleaders.com.au/storage
sudo cp ~/linode/hello-leaders-management/horizon/horizon-my.helloleaders.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/hello-leaders-management/env/my.helloleaders.com.au /var/www/my.helloleaders.com.au/.env

sudo cp /home/dps/linode/hello-leaders-management/nginx/my.helloleaders.com.au.conf /etc/nginx/sites-available/my.helloleaders.com.au.conf
sudo ln -s /etc/nginx/sites-available/my.helloleaders.com.au.conf /etc/nginx/sites-enabled/my.helloleaders.com.au.conf

# Hello Leaders (Preview) - preview.my.helloleaders.com.au

mkdir /var/www/preview.my.helloleaders.com.au \
  /var/www/preview.my.helloleaders.com.au/public \
  /var/www/preview.my.helloleaders.com.au/releases \
  /var/www/preview.my.helloleaders.com.au/storage \
  /var/www/preview.my.helloleaders.com.au/storage/app \
  /var/www/preview.my.helloleaders.com.au/storage/app/public \
  /var/www/preview.my.helloleaders.com.au/storage/framework \
  /var/www/preview.my.helloleaders.com.au/storage/framework/cache \
  /var/www/preview.my.helloleaders.com.au/storage/framework/cache/data \
  /var/www/preview.my.helloleaders.com.au/storage/framework/sessions \
  /var/www/preview.my.helloleaders.com.au/storage/framework/testing \
  /var/www/preview.my.helloleaders.com.au/storage/framework/views \
  /var/www/preview.my.helloleaders.com.au/storage/logs

chmod -R 775 /var/www/preview.my.helloleaders.com.au/storage
sudo cp ~/linode/hello-leaders-management/horizon/horizon-preview.my.helloleaders.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/hello-leaders-management/env/preview.my.helloleaders.com.au /var/www/preview.my.helloleaders.com.au/.env

sudo cp /home/dps/linode/hello-leaders-management/nginx/preview.my.helloleaders.com.au.conf /etc/nginx/sites-available/preview.my.helloleaders.com.au.conf
sudo ln -s /etc/nginx/sites-available/preview.my.helloleaders.com.au.conf /etc/nginx/sites-enabled/preview.my.helloleaders.com.au.conf

####################################################
## Supervisor (Restart)
###################################################

sudo systemctl restart supervisor

####################################################
## TLS
####################################################

sudo cp /home/dps/linode/hello-leaders-management/tls/cloudflare.pem /etc/ssl/certs/cloudflare.pem
sudo cp /home/dps/linode/hello-leaders-management/tls/cloudflare.key /etc/ssl/private/cloudflare.key

sudo systemctl restart nginx

####################################################
## Iptables - block HTTP/S access
####################################################

#sudo iptables -A INPUT -p tcp -m tcp -m multiport --dports 22,80,443 -j ACCEPT
#sudo iptables -A INPUT -m conntrack -j ACCEPT  --ctstate RELATED,ESTABLISHED
#sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A INPUT -j DROP
#sudo iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A OUTPUT -j DROP
#sudo iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A FORWARD -j DROP

# Add iptables helper methods to bashrc
cat ~/linode/hello-leaders-management/bashrc-helpers >> ~/.bashrc

####################################################
## Misc
####################################################

# Set hostname
echo "host-01.management.helloleaders.com.au" | sudo tee /etc/hostname

####################################################
## Finishing
###################################################

echo "-------------------------------------------"
echo "Please don't forget to reboot this machine"
echo "for all security updates to take hold."
echo "-------------------------------------------"


#Extra Notes
#
#Umask 0002
#Create file at /etc/systemd/system/php8.1-fpm.service.d/override.conf
#Add the following two lines
#[Service]
#UMask=0002
#Then systemctl daemon-reload && systemctl restart php8.1-fpm
#
#crontab
#* * * * * umask 002 && cd /var/www/admin.helloleaders.com.au/current && php artisan schedule:run >> /dev/null 2>&1
#* * * * * umask 002 && cd /var/www/preview.admin.helloleaders.com.au/current && php artisan schedule:run >> /dev/null 2>&1
