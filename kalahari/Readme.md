Linode Config
- Ubuntu 22.04 LTS
- Region Sydney, AU
- Shared CPU Linode 1 GB ($5/month, $0.015/hour)
- Label YYYYMMDD-kalahari
- Root password xPSeTVHr8pVwHX5stB
- With private IP

Step One:

Update 172.105.174.109 A record to VPS IP Address
https://dash.cloudflare.com/43c04c4261b904a7a84467d8950b7b9c/dps.com.au/dns

Step Two:

./init.sh 1.2.3.4

Step Three:

ssh dps@host.cms.dps.com.au
cd linode/kalahari
./setup.sh
Script is not completely non-interactive, some confirmations are required.

Step Four:

sudo reboot

Step Five:

Deploy each app
envoy run deploy --uc
Omit --uc if you wish to deploy master branch.
