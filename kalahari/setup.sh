#!/bin/bash

####################################################
## SETUP.SH - PROVISION THE HOST                  ##
####################################################

set -e
clear

export DEBIAN_FRONTEND=noninteractive

####################################################
## Update the host
####################################################

sudo apt-get update
sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages full-upgrade

####################################################
## Unattended Upgrades
####################################################

sudo apt-get install -y unattended-upgrades update-notifier-common
sudo systemctl enable unattended-upgrades
sudo systemctl restart unattended-upgrades

echo 'Unattended-Upgrade::Mail "cto@dps.com.au";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::MailReport  "on-change";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-WithUsers "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-Time "20:00";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades

####################################################
## Install generic tools
####################################################

sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages install pv software-properties-common wget

####################################################
## MySQL Client
####################################################

sudo apt-get install -y mysql-client

####################################################
## Nginx - Installation
####################################################

sudo apt-get install -y nginx

sudo chmod 775 /var/www
sudo chown -R dps /var/www
sudo chgrp -R web /var/www
sudo chmod g+s /var/www

sudo sed -i "s/.*# server_tokens off.*/        server_tokens off;/g" /etc/nginx/nginx.conf
sudo systemctl restart nginx.service

echo "* * * * * /home/dps/linode/kalahari/nginx-restart.sh > /dev/null 2>&1" | crontab -

####################################################
## PHP (v8.1)
####################################################

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php8.1-fpm php8.1-imagick php8.1-cli php8.1-xml php8.1-curl \
  php8.1-redis php8.1-dom php8.1-mbstring php8.1-zip php8.1-gd \
  php8.1-mysql php8.1-common php8.1-intl php8.1-opcache php8.1-dev php8.1-bcmath

####################################################
## Nginx Virtual Hosts
###################################################

mkdir /var/www/wordpress;
sudo cp /home/dps/linode/kalahari/nginx/wordpress.conf /etc/nginx/sites-available/wordpress.conf
sudo ln -s /etc/nginx/sites-available/wordpress.conf /etc/nginx/sites-enabled/wordpress.conf

####################################################
## Wordpress CLI
####################################################

sudo apt-get install unzip

cd ~
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

cd /var/www/wordpress
wp core download

read -sp 'DB Password: ' dbpass
read -sp 'Admin Password: ' adminpass

wp config create --dbhost=192.168.134.17 --dbname=dps_cms --dbuser=dps_cms --dbpass=$dbpass
wp core install --url=cms.dps.com.au --title=Kalahari --admin_user=super --admin_password=$adminpass --admin_email=adam.lehmann@dps.com.au
wp plugin install classic-editor headless-mode jwt-authentication-for-wp-rest-api
####################################################
## Wordpress Permissions
####################################################

sudo chown -R www-data:dps /var/www/wordpress
sudo chmod -R 775 /var/www/wordpress
#sudo chmod 664 index.php
#sudo chmod 640 wp-config.php

####################################################
## TLS
####################################################

sudo cp /home/dps/linode/kalahari/tls/cloudflare.pem /etc/ssl/certs/cloudflare.pem
sudo cp /home/dps/linode/kalahari/tls/cloudflare.key /etc/ssl/private/cloudflare.key

sudo systemctl restart nginx

####################################################
## Iptables - block HTTP/S access
####################################################

# Add iptables helper methods to bashrc
cat ~/linode/kalahari/bashrc-helpers >> ~/.bashrc

####################################################
## Misc
####################################################

# Set hostname
echo "host.cms.dps.com.au" | sudo tee /etc/hostname

####################################################
## Finishing
###################################################

echo "-------------------------------------------"
echo "Please don't forget to reboot this machine"
echo "for all security updates to take hold."
echo "-------------------------------------------"
