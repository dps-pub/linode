#!/bin/bash

####################################################
## SETUP.SH - PROVISION THE HOST                  ##
####################################################

set -e
clear

export DEBIAN_FRONTEND=noninteractive

####################################################
## Validate arguments
####################################################

if [ -z "$1" ]
  then
    echo "No private ip supplied."
    exit 1
fi

PRIVATE_IP=$1

####################################################
## Update the host
####################################################

sudo apt-get update
sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages full-upgrade

####################################################
## Unattended Upgrades
####################################################

sudo apt-get install -y unattended-upgrades update-notifier-common
sudo systemctl enable unattended-upgrades
sudo systemctl restart unattended-upgrades

echo 'Unattended-Upgrade::Mail "cto@dps.com.au";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::MailReport  "on-change";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-WithUsers "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-Time "20:00";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades

####################################################
## Install generic tools
####################################################

sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages install pv software-properties-common supervisor unzip

####################################################
## MySQL (v8)
####################################################

## https://chartio.com/resources/tutorials/how-to-grant-all-privileges-on-a-database-in-mysql/#saving-your-changes
sudo apt-get install -y mysql-server
sudo sed -i "s/^bind-address.*/bind-address = 127.0.0.1,$PRIVATE_IP/g" /etc/mysql/mysql.conf.d/mysqld.cnf
sudo systemctl enable mysql.service
sudo systemctl restart mysql.service

sudo mysql -e "CREATE USER 'general'@'%' IDENTIFIED BY 'password';"
sudo mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'general'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

####################################################
## Redis
####################################################

sudo apt-get install -y redis-server
sudo sed -i 's/^supervised no/supervised systemd/g' /etc/redis/redis.conf
sudo systemctl restart redis

####################################################
## Docker
####################################################

#sudo apt-get install -y docker.io

####################################################
## Docker Compose
####################################################

#sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
#sudo chmod +x /usr/local/bin/docker-compose

####################################################
## Nginx - Installation
####################################################

sudo apt-get install -y nginx certbot python3-certbot-nginx
sudo chmod 775 /var/www
sudo chown -R dps /var/www
sudo chgrp -R web /var/www
sudo chmod g+s /var/www

sudo sed -i "s/.*# server_tokens off.*/        server_tokens off;/g" /etc/nginx/nginx.conf
sudo systemctl restart nginx.service

echo "* * * * * /home/dps/linode/staging/nginx-restart.sh > /dev/null 2>&1" | crontab -

####################################################
## Nodejs
###################################################

curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs

####################################################
## Font Awesome Pro
###################################################

npm config set "@fortawesome:registry" https://npm.fontawesome.com/
npm config set "//npm.fontawesome.com/:_authToken" 33D79871-BE5F-4E96-ABAE-EF7231039D95

####################################################
## PHP (v8.1)
###################################################

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php8.1-fpm php8.1-imagick php8.1-cli php8.1-xml php8.1-curl \
  php8.1-redis php8.1-dom php8.1-mbstring php8.1-zip php8.1-gd \
  php8.1-mysql php8.1-common php8.1-intl php8.1-opcache php8.1-dev php8.1-bcmath

####################################################
## PHP Composer
###################################################

curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo  php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php

####################################################
## Craft CMS
###################################################

#git clone git@gitlab.com:dps-pub/craft.git /var/www/craft
#git -C /var/www/craft checkout linode-staging
#
#git -C /var/www/craft checkout linode-production
#
#touch /var/www/craft/.env
#sudo docker-compose -f /var/www/craft/docker-compose.yml up -d
#sudo cp ~/linode/staging/docker-compose-app.service /etc/systemd/system/docker-compose-app.service

####################################################
## Nginx Virtual Hosts
###################################################

# Sahara - stg-01.sahara

mkdir /var/www/stg-01.sahara \
  /var/www/stg-01.sahara/public \
  /var/www/stg-01.sahara/releases \
  /var/www/stg-01.sahara/storage \
  /var/www/stg-01.sahara/storage/app \
  /var/www/stg-01.sahara/storage/app/public \
  /var/www/stg-01.sahara/storage/framework \
  /var/www/stg-01.sahara/storage/framework/cache \
  /var/www/stg-01.sahara/storage/framework/cache/data \
  /var/www/stg-01.sahara/storage/framework/sessions \
  /var/www/stg-01.sahara/storage/framework/testing \
  /var/www/stg-01.sahara/storage/framework/views \
  /var/www/stg-01.sahara/storage/logs

chmod -R 775 /var/www/stg-01.sahara/storage
sudo cp ~/linode/staging/horizon/horizon-stg-01-sahara.conf /etc/supervisor/conf.d/

cp ~/linode/staging/env/sahara /var/www/stg-01.sahara/.env
sed -i 's/SAHARA_DOMAIN/stg-01.sahara.dev.dps.com.au/g' /var/www/stg-01.sahara/.env
sed -i 's/MY_GUIDE_DOMAIN/stg-01.my-guide.dev.dps.com.au/g' /var/www/stg-01.sahara/.env

sudo cp /home/dps/linode/staging/nginx/stg-01.sahara.conf /etc/nginx/sites-available/stg-01.sahara.conf
sudo ln -s /etc/nginx/sites-available/stg-01.sahara.conf /etc/nginx/sites-enabled/stg-01.sahara.conf

# My Guide - stg-01.my-guide

mkdir /var/www/stg-01.my-guide \
  /var/www/stg-01.my-guide/public \
  /var/www/stg-01.my-guide/releases \
  /var/www/stg-01.my-guide/storage \
  /var/www/stg-01.my-guide/storage/app \
  /var/www/stg-01.my-guide/storage/app/public \
  /var/www/stg-01.my-guide/storage/framework \
  /var/www/stg-01.my-guide/storage/framework/cache \
  /var/www/stg-01.my-guide/storage/framework/cache/data \
  /var/www/stg-01.my-guide/storage/framework/sessions \
  /var/www/stg-01.my-guide/storage/framework/testing \
  /var/www/stg-01.my-guide/storage/framework/views \
  /var/www/stg-01.my-guide/storage/logs

chmod -R 775 /var/www/stg-01.my-guide/storage

sudo cp ~/linode/staging/horizon/horizon-stg-01-my-guide.conf /etc/supervisor/conf.d/

cp ~/linode/staging/env/my-guide /var/www/stg-01.my-guide/.env
sed -i 's/MY_GUIDE_DOMAIN/stg-01.my-guide.dev.dps.com.au/g' /var/www/stg-01.my-guide/.env

sudo cp /home/dps/linode/staging/nginx/stg-01.my-guide.conf /etc/nginx/sites-available/stg-01.my-guide.conf
sudo ln -s /etc/nginx/sites-available/stg-01.my-guide.conf /etc/nginx/sites-enabled/stg-01.my-guide.conf

# Aged Care Guide - stg-01.aged-care-guide

mkdir /var/www/stg-01.aged-care-guide \
  /var/www/stg-01.aged-care-guide/public \
  /var/www/stg-01.aged-care-guide/releases \
  /var/www/stg-01.aged-care-guide/storage \
  /var/www/stg-01.aged-care-guide/storage/app \
  /var/www/stg-01.aged-care-guide/storage/app/public \
  /var/www/stg-01.aged-care-guide/storage/framework \
  /var/www/stg-01.aged-care-guide/storage/framework/cache \
  /var/www/stg-01.aged-care-guide/storage/framework/cache/data \
  /var/www/stg-01.aged-care-guide/storage/framework/sessions \
  /var/www/stg-01.aged-care-guide/storage/framework/testing \
  /var/www/stg-01.aged-care-guide/storage/framework/views \
  /var/www/stg-01.aged-care-guide/storage/logs

chmod -R 775 /var/www/stg-01.aged-care-guide/storage

sudo cp ~/linode/staging/horizon/horizon-stg-01-aged-care-guide.conf /etc/supervisor/conf.d/

cp ~/linode/staging/env/aged-care-guide /var/www/stg-01.aged-care-guide/.env
sed -i 's/AGED_CARE_GUIDE_DOMAIN/stg-01.aged-care-guide.dev.dps.com.au/g' /var/www/stg-01.aged-care-guide/.env
sed -i 's/MY_GUIDE_DOMAIN/stg-01.my-guide.dev.dps.com.au/g' /var/www/stg-01.aged-care-guide/.env

sudo cp /home/dps/linode/staging/nginx/stg-01.aged-care-guide.conf /etc/nginx/sites-available/stg-01.aged-care-guide.conf
sudo ln -s /etc/nginx/sites-available/stg-01.aged-care-guide.conf /etc/nginx/sites-enabled/stg-01.aged-care-guide.conf

# DSG - stg-01.dsg

mkdir /var/www/stg-01.dsg \
  /var/www/stg-01.dsg/public \
  /var/www/stg-01.dsg/releases \
  /var/www/stg-01.dsg/storage \
  /var/www/stg-01.dsg/storage/app \
  /var/www/stg-01.dsg/storage/app/public \
  /var/www/stg-01.dsg/storage/framework \
  /var/www/stg-01.dsg/storage/framework/cache \
  /var/www/stg-01.dsg/storage/framework/cache/data \
  /var/www/stg-01.dsg/storage/framework/sessions \
  /var/www/stg-01.dsg/storage/framework/testing \
  /var/www/stg-01.dsg/storage/framework/views \
  /var/www/stg-01.dsg/storage/logs

chmod -R 775 /var/www/stg-01.dsg/storage

sudo cp ~/linode/staging/horizon/horizon-stg-01-dsg.conf /etc/supervisor/conf.d/

cp ~/linode/staging/env/dsg /var/www/stg-01.dsg/.env
sed -i 's/AGED_CARE_GUIDE_DOMAIN/stg-01.aged-care-guide.dev.dps.com.au/g' /var/www/stg-01.dsg/.env
sed -i 's/DSG_DOMAIN/stg-01.dsg.dev.dps.com.au/g' /var/www/stg-01.dsg/.env

sudo cp /home/dps/linode/staging/nginx/stg-01.dsg.conf /etc/nginx/sites-available/stg-01.dsg.conf
sudo ln -s /etc/nginx/sites-available/stg-01.dsg.conf /etc/nginx/sites-enabled/stg-01.dsg.conf


# Kalahari - stg-01.kalahari

mkdir /var/www/stg-01.kalahari;
sudo cp /home/dps/linode/staging/nginx/stg-01.kalahari.conf /etc/nginx/sites-available/stg-01.kalahari.conf
sudo ln -s /etc/nginx/sites-available/stg-01.kalahari.conf /etc/nginx/sites-enabled/stg-01.kalahari.conf

####################################################
## Supervisor (Restart)
###################################################

sudo systemctl restart supervisor

####################################################
## Nginx (Complete and HTTPS Certificates)
###################################################

sudo systemctl restart nginx

sudo certbot \
  --nginx \
  --non-interactive \
  --agree-tos \
  -m cto@dps.com.au \
  -d stg-01.sahara.dev.dps.com.au \
  -d stg-01.my-guide.dev.dps.com.au \
  -d stg-01.aged-care-guide.dev.dps.com.au \
  -d stg-01.dsg.dev.dps.com.au \
  -d stg-01.kalahari.dev.dps.com.au

####################################################
## Wordpress CLI
####################################################

cd ~
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

cd /var/www/stg-01.kalahari
wp core download

read -sp 'DB Password: ' dbpass
read -sp 'Admin Password: ' adminpass

wp config create --dbhost=127.0.0.1 --dbname=kalahari --dbuser=general --dbpass=$dbpass
wp core install --url=stg-01.kalahari.dev.dps.com.au --title=Kalahari --admin_user=super --admin_password=$adminpass --admin_email=adam.lehmann@dps.com.au
wp plugin install classic-editor headless-mode jwt-authentication-for-wp-rest-api

####################################################
## Wordpress Permissions
####################################################

sudo chown -R www-data:dps /var/www/stg-01.kalahari
sudo chmod -R 775 /var/www/stg-01.kalahari

####################################################
## Iptables - block HTTP/S access
####################################################

## Localhost
sudo iptables -A INPUT -p tcp --dport 80 -s 127.0.0.1 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -s 127.0.0.1 -j ACCEPT

## TODO add public IP
#sudo iptables -A INPUT -p tcp --dport 80 -s 172.105.181.16 -j ACCEPT
#sudo iptables -A INPUT -p tcp --dport 443 -s 172.105.181.16 -j ACCEPT

## 145 South Terrace
sudo iptables -A INPUT -p tcp --dport 80 -s 61.68.31.82 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -s 61.68.31.82 -j ACCEPT

## Block all other 80/443 connections
sudo iptables -A INPUT -p tcp --dport 80 -j DROP
sudo iptables -A INPUT -p tcp --dport 443 -j DROP

## Persist rules after reboot
apt-get install -y iptables-persistent
iptables-save > /etc/iptables/rules.v4
iptables-save > /etc/iptables/rules.v6

# Add iptables helper methods to bashrc
cat ~/linode/staging/bashrc-helpers >> ~/.bashrc

####################################################
## Finishing
###################################################

echo "-------------------------------------------"
echo "Please don't forget to reboot this machine"
echo "for all security updates to take hold."
echo "-------------------------------------------"
