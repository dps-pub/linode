Linode Config
- Ubuntu 20.04 LTS
- Region Sydney, AU
- Shared CPU Linode 8 GB ($40/month, $0.06/hour)
- Label YYYYMMDD-website-staging
- Root password o6ffebJ3Y1nRiLQlID
- With private IP address

root password
o6ffebJ3Y1nRiLQlID

IP
172.105.163.190

Step One:
Update staging.dev.dps.com.au A record to VPS IP Address
https://dash.cloudflare.com/43c04c4261b904a7a84467d8950b7b9c/dps.com.au/dns

Step One:
./init.sh 1.2.3.4

Step Two:
ssh dps@staging.dev.dps.com.au
cd linode/staging
./setup.sh PRIVATE_IP
Script is not completely non-interactive, some confirmations are required.

Step Four:
sudo reboot

Step Five:
ssh in and make sure that craft is running
ssh dps@staging.dev.dps.com.au
sudo docker ps
if it's not then run
sudo docker-compose -f /var/www/craft/docker-compose.yml up -d

Step Six:
Copy databases to staging host
scp ~/code/linode/staging/database/*.sql.gz dps@staging.dev.dps.com.au:~/linode/staging/database/

Step Seven:
ssh dps@staging.dev.dps.com.au
cd ~/linode/staging && ./import-database.sh 20221021
The date of the imports to be supplied

Step Six:
Deploy each app
envoy run deploy --uc
Omit --uc if you wish to deploy master branch.






Craft can be viewed at
http://172.105.163.190:4431/


# Accessing craft dataqbase
mysql -u root -ppassword -h 172.18.0.1 --port=33306

# Import craft database
pv ~/craft_20210902.sql.gz | zcat | mysql -u root -ppassword -h 172.18.0.1 --port=33306 craft


scp ~/code/linode/staging/database/*.sql.gz dps@staging.dev.dps.com.au:~/linode/staging/database/


# Export craft database

enter mysql container
docker ps
docker exec -it XXXX bash
mysqldump -u root -ppassword craft > /craft-export.sql

back in linode
sudo docker cp 86a0:/craft-export.sql ~/craft-export.sql
sudo chown dps:web ~/craft-export.sql

on local machine
scp dps@staging.dev.dps.com.au:~/craft-export.sql ~/Downloads/staging-craft-export.sql

Import into local machine
mysql -u general -ppassword -e "DROP DATABASE IF EXISTS staging_craft; CREATE DATABASE staging_craft;"
pv ~/Downloads/staging-craft-export.sql | mysql -u general -ppassword staging_craft





mysqldump -u root -ppassword -h 172.18.0.1 -P 33306 craft > ~/craft-export.sql
scp dps@staging.dev.dps.com.au:~/craft-export.sql ~/Downloads/craft-export.sql