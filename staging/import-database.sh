#!/bin/bash

####################################################
## Import Databases
##
## Prerequisite:
## Copy over minified databases from local to staging
## scp ~/code/linode/staging/database/*.sql.gz dps@staging.dev.dps.com.au:~/linode/staging/database/
##
## Example:
## ./import-database.sh 20210914
####################################################

DATE=$1

# CRAFT_DB_NAME=craft_$DATE
NDIS_DB_NAME=ndis_$DATE
ACG_DB_NAME=acg_$DATE

# mysql -u general -ppassword -e "CREATE DATABASE IF NOT EXISTS $CRAFT_DB_NAME;"
mysql -u general -ppassword -e "CREATE DATABASE IF NOT EXISTS $NDIS_DB_NAME;"
mysql -u general -ppassword -e "CREATE DATABASE IF NOT EXISTS $ACG_DB_NAME;"

#pv ~/linode/staging/database/$CRAFT_DB_NAME.sql.gz | zcat | mysql -u root -ppassword -h 172.18.0.1 --port=33306 craft
# pv ~/linode/staging/database/$CRAFT_DB_NAME.sql.gz | zcat | mysql -u general -ppassword $CRAFT_DB_NAME
pv ~/linode/staging/database/$NDIS_DB_NAME.sql.gz | zcat | mysql -u general -ppassword $NDIS_DB_NAME
pv ~/linode/staging/database/$ACG_DB_NAME.sql.gz | zcat | mysql -u general -ppassword $ACG_DB_NAME