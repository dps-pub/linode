#!/bin/bash

####################################################
## SETUP.SH - PROVISION THE HOST                  ##
####################################################

set -e
clear

export DEBIAN_FRONTEND=noninteractive

####################################################
## Update the host
####################################################

sudo apt-get update
sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages full-upgrade

####################################################
## Unattended Upgrades
####################################################

sudo apt-get install -y unattended-upgrades update-notifier-common
sudo systemctl enable unattended-upgrades
sudo systemctl restart unattended-upgrades

echo 'Unattended-Upgrade::Mail "cto@dps.com.au";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::MailReport  "on-change";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-WithUsers "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-Time "20:00";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades

####################################################
## Install generic tools
####################################################

sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages install pv software-properties-common supervisor

####################################################
## MySQL Client
####################################################

sudo apt-get install -y mysql-client

####################################################
## Redis
####################################################

sudo apt-get install -y redis-server
sudo sed -i 's/^supervised no/supervised systemd/g' /etc/redis/redis.conf
sudo systemctl restart redis

####################################################
## Nginx - Installation
####################################################

sudo apt-get install -y nginx certbot python3-certbot-nginx

sudo chmod 775 /var/www
sudo chown -R dps /var/www
sudo chgrp -R web /var/www
sudo chmod g+s /var/www

sudo sed -i "s/.*# server_tokens off.*/        server_tokens off;/g" /etc/nginx/nginx.conf
sudo systemctl restart nginx.service

echo "* * * * * /home/dps/linode/app-analytics/nginx-restart.sh > /dev/null 2>&1" | crontab -

####################################################
## Nodejs
###################################################

curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs

####################################################
## Font Awesome Pro
###################################################

npm config set "@fortawesome:registry" https://npm.fontawesome.com/
npm config set "//npm.fontawesome.com/:_authToken" 33D79871-BE5F-4E96-ABAE-EF7231039D95

####################################################
## PHP (v7.4)
###################################################

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php7.4-fpm php7.4-imagick php7.4-cli php7.4-xml php7.4-curl \
  php7.4-redis php7.4-dom php7.4-json php7.4-mbstring php7.4-zip php7.4-gd \
  php7.4-mysql php7.4-common php7.4-intl php7.4-opcache php7.4-dev php7.4-bcmath

####################################################
## PHP (v8.0)
###################################################

#sudo add-apt-repository ppa:ondrej/php
#sudo apt-get update
#sudo apt-get install -y php8.0-fpm php8.0-imagick php8.0-cli php8.0-xml php8.0-curl \
#  php8.0-redis php8.0-dom php8.0-mbstring php8.0-zip php8.0-gd \
#  php8.0-mysql php8.0-common php8.0-intl php8.0-opcache php8.0-dev php8.0-bcmath

####################################################
## PHP Composer
###################################################

curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo  php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php

####################################################
## Nginx Virtual Hosts
###################################################

# Analytics - analytics.dps.com.au

mkdir /var/www/analytics.dps.com.au \
  /var/www/analytics.dps.com.au/public \
  /var/www/analytics.dps.com.au/releases \
  /var/www/analytics.dps.com.au/storage \
  /var/www/analytics.dps.com.au/storage/app \
  /var/www/analytics.dps.com.au/storage/app/public \
  /var/www/analytics.dps.com.au/storage/framework \
  /var/www/analytics.dps.com.au/storage/framework/cache \
  /var/www/analytics.dps.com.au/storage/framework/cache/data \
  /var/www/analytics.dps.com.au/storage/framework/sessions \
  /var/www/analytics.dps.com.au/storage/framework/testing \
  /var/www/analytics.dps.com.au/storage/framework/views \
  /var/www/analytics.dps.com.au/storage/logs

chmod -R 775 /var/www/analytics.dps.com.au/storage
sudo cp ~/linode/app-analytics/horizon/horizon-analytics.dps.com.au.conf /etc/supervisor/conf.d/

cp ~/linode/app-analytics/env/analytics.dps.com.au /var/www/analytics.dps.com.au/.env

sudo cp /home/dps/linode/app-analytics/nginx/analytics.dps.com.au.conf /etc/nginx/sites-available/analytics.dps.com.au.conf
sudo ln -s /etc/nginx/sites-available/analytics.dps.com.au.conf /etc/nginx/sites-enabled/analytics.dps.com.au.conf

####################################################
## Supervisor (Restart)
###################################################

sudo systemctl restart supervisor

####################################################
## Nginx (Complete and HTTPS Certificates)
###################################################

sudo systemctl restart nginx

sudo certbot \
  --nginx \
  --non-interactive \
  --agree-tos \
  --expand \
  -m cto@dps.com.au \
  -d analytics.dps.com.au

####################################################
## Iptables - block HTTP/S access
####################################################

#sudo iptables -A INPUT -p tcp -m tcp -m multiport --dports 22,80,443 -j ACCEPT
#sudo iptables -A INPUT -m conntrack -j ACCEPT  --ctstate RELATED,ESTABLISHED
#sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A INPUT -j DROP
#sudo iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A OUTPUT -j DROP
#sudo iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A FORWARD -j DROP

# Add iptables helper methods to bashrc
cat ~/linode/app-analytics/bashrc-helpers >> ~/.bashrc

####################################################
## Misc
####################################################

# Set hostname
echo "app-analytics.dps.com.au" | sudo tee /etc/hostname

####################################################
## Finishing
###################################################

echo "-------------------------------------------"
echo "Please don't forget to reboot this machine"
echo "for all security updates to take hold."
echo "-------------------------------------------"
