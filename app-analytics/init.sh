#!/bin/bash

####################################################
## INIT.SH
#  User provisioning for new host and adds linode git repo to remote host.
## Run locally on user machine.
##
## Example (note VPS remote IP is required):
## ./init.sh 1.2.3.4
####################################################

set -e
clear

IP_ADDRESS=$1

echo "Copying over SSH files."

scp ssh/* root@$IP_ADDRESS:~/.ssh/

echo "Setting up ssh files and DPS user."

ssh root@$IP_ADDRESS << EOF
  chmod 700 ~/.ssh
  chmod 644 ~/.ssh/authorized_keys
  chmod 600 ~/.ssh/id_ed25519
  chmod 644 ~/.ssh/id_ed25519.pub
  chmod 644 ~/.ssh/known_hosts

  groupadd web
  useradd -m dps
  usermod -a -G web www-data
  usermod -g web dps
  usermod -a -G docker dps
  usermod -a -G sudo dps
  usermod --shell /bin/bash dps
  echo "dps:kEUkBVR4veVc" | chpasswd
  echo "dps     ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

  cp -R ~/.ssh /home/dps/.ssh
  chown -R dps /home/dps/.ssh
  chgrp -R dps /home/dps/.ssh
  chmod 700 /home/dps/.ssh
  chmod 644 /home/dps/.ssh/authorized_keys
  chmod 600 /home/dps/.ssh/id_ed25519
  chmod 644 /home/dps/.ssh/id_ed25519.pub
  chmod 644 /home/dps/.ssh/known_hosts

  vim -c ":a" -c ":wq" /home/dps/.ssh/id_ed25519

  sed -i 's/UMASK           011/UMASK           003/g' /etc/login.defs
EOF

echo "Checking ssh for dps user works."

ssh dps@$IP_ADDRESS << EOF
  echo "You are now ssh'd into the remote host ($IP_ADDRESS) as " `whoami`
  git clone git@gitlab.com:dps-pub/linode.git
  mkdir -p ~/.config/composer
  cp ~/linode/staging/composer/auth.json ~/.config/composer/auth.json
  exit
EOF

echo "You can now SSH in as the dps user."

