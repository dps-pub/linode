Linode Config
- Ubuntu 20.04 LTS
- Region Sydney, AU
- Shared CPU Linode 2 GB ($10/month, $0.015/hour)
- Label YYYYMMDD-analytics
- Root password jM9PvPSboUR3SKhPTJ
- With private IP

Step One:

Update app-analytics.dps.com.au A record to VPS IP Address
https://dash.cloudflare.com/43c04c4261b904a7a84467d8950b7b9c/dps.com.au/dns

Step Two:

./init.sh 1.2.3.4

Step Three:

ssh dps@app-analytics.dps.com.au
cd linode/app-analytics
./setup.sh
Script is not completely non-interactive, some confirmations are required.

Step Four:

sudo reboot

Step Five:

Deploy each app
envoy run deploy --uc
Omit --uc if you wish to deploy master branch.
