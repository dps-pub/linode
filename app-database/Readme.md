Linode Config
- Ubuntu 20.04 LTS
- Region Sydney, AU
- Shared CPU Linode 2 GB ($10/month, $0.015/hour)
- Label YYYYMMDD-database
- Root password zxrKWD2wRmqZRvK
- With private IP

Step One:

Update app-database.dps.com.au A record to VPS IP Address
Update app-database.private.dps.com.au A record to VPS IP Address
https://dash.cloudflare.com/43c04c4261b904a7a84467d8950b7b9c/dps.com.au/dns

Step Two:

./init.sh 1.2.3.4

Step Three:

ssh dps@app-database.dps.com.au
cd linode/app-database
./setup.sh PRIVATE_IP APP_ANALYTICS_PRIVATE_IP
Script is not completely non-interactive, some confirmations are required.

Step Four:

sudo reboot
