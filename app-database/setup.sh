#!/bin/bash

####################################################
## SETUP.SH - PROVISION THE HOST                  ##
####################################################

set -e
clear

export DEBIAN_FRONTEND=noninteractive

####################################################
## Validate arguments
####################################################

if [ -z "$1" ]
  then
    echo "No private ip supplied."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No analytics application private ip supplied."
    exit 1
fi

PRIVATE_IP=$1
APP_ANALYTICS_PRIVATE_IP=$2

####################################################
## Update the host
####################################################

sudo apt-get update
sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages full-upgrade

####################################################
## Unattended Upgrades
####################################################

sudo apt-get install -y unattended-upgrades update-notifier-common
sudo systemctl enable unattended-upgrades
sudo systemctl restart unattended-upgrades

echo 'Unattended-Upgrade::Mail "cto@dps.com.au";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::MailReport  "on-change";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-WithUsers "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-Time "20:00";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades

####################################################
## Install generic tools
####################################################

sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages install pv software-properties-common

####################################################
## MySQL (v8)
####################################################

## https://chartio.com/resources/tutorials/how-to-grant-all-privileges-on-a-database-in-mysql/#saving-your-changes
sudo apt-get install -y mysql-server
sudo sed -i "s/^bind-address.*/bind-address = 127.0.0.1,$PRIVATE_IP/g" /etc/mysql/mysql.conf.d/mysqld.cnf
echo "* * * * * /home/dps/linode/app-database/mysql-restart.sh > /dev/null 2>&1" | crontab -
sudo systemctl enable mysql.service
sudo systemctl restart mysql.service

sudo mysql -e "CREATE DATABASE analytics;"
sudo mysql -e "CREATE USER 'analytics'@'%' IDENTIFIED BY 'A4vODXvb7bGyfFWg';"
sudo mysql -e "GRANT ALL PRIVILEGES ON analytics.* TO 'analytics'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

sudo mysql -e "CREATE DATABASE aws_monitor;"
sudo mysql -e "CREATE USER 'aws_monitor'@'%' IDENTIFIED BY 'yJKbTeGD4HDgJPaU';"
sudo mysql -e "GRANT ALL PRIVILEGES ON aws_monitor.* TO 'aws_monitor'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

sudo mysql -e "CREATE DATABASE helloleaders_au_wp;"
sudo mysql -e "CREATE USER 'helloleaders_au_wp'@'%' IDENTIFIED BY 'IeQ5xUGvW5MZZYT';"
sudo mysql -e "GRANT ALL PRIVILEGES ON helloleaders_au_wp.* TO 'helloleaders_au_wp'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

sudo mysql -e "CREATE DATABASE dps_news_wp;"
sudo mysql -e "CREATE USER 'dps_news_wp'@'%' IDENTIFIED BY 'zdoPLXKyqY5';"
sudo mysql -e "GRANT ALL PRIVILEGES ON dps_news_wp.* TO 'dps_news_wp'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

sudo mysql -e "CREATE DATABASE helloleaders_au_app_stg;"
sudo mysql -e "CREATE USER 'helloleaders_au_app_stg'@'%' IDENTIFIED BY 'Nv0UCxe7zlGcOJ4';"
sudo mysql -e "GRANT ALL PRIVILEGES ON helloleaders_au_app_stg.* TO 'helloleaders_au_app_stg'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

sudo mysql -e "CREATE DATABASE helloleaders_au_app_prod;"
sudo mysql -e "CREATE USER 'helloleaders_au_app_prod'@'%' IDENTIFIED BY 'yJGBtC1qaVAyeH68VKnW2G';"
sudo mysql -e "GRANT ALL PRIVILEGES ON helloleaders_au_app_prod.* TO 'helloleaders_au_app_prod'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

sudo mysql -e "CREATE DATABASE dps_cms;"
sudo mysql -e "CREATE USER 'dps_cms'@'%' IDENTIFIED BY 'M6Ngjw84yjhEdx6s';"
sudo mysql -e "GRANT ALL PRIVILEGES ON dps_cms.* TO 'dps_cms'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"

sudo mysql -e "CREATE DATABASE mailcoach;"
sudo mysql -e "CREATE USER 'mailcoach'@'%' IDENTIFIED BY 'VmKYHrc1Fr0tsYEy';"
sudo mysql -e "GRANT ALL PRIVILEGES ON mailcoach.* TO 'mailcoach'@'%';"
sudo mysql -e "FLUSH PRIVILEGES;"


####################################################
## Iptables - block mysql access
####################################################

sudo iptables -A INPUT -p tcp --dport 3306 -s 127.0.0.1 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 3306 -s $APP_ANALYTICS_PRIVATE_IP -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 3306 -j DROP

# Add iptables helper methods to bashrc
cat ~/linode/app-database/bashrc-helpers >> ~/.bashrc

####################################################
## Misc
####################################################

# Set hostname
echo "app-database.dps.com.au" | sudo tee /etc/hostname

####################################################
## Finishing
###################################################

echo "-------------------------------------------"
echo "Please don't forget to reboot this machine"
echo "for all security updates to take hold."
echo "-------------------------------------------"
