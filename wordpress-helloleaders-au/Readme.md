Linode Config
- Ubuntu 20.04 LTS
- Region Sydney, AU
- Shared CPU Nanode 1GB ($5/month, $0.0075/hour)
- Label YYYYMMDD-wordpress-helloleaders-au
- Root password V3BSROsdfdfopU
- With private IP

Step One:

Update host.wp.helloleaders.com.au A record to VPS IP Address
https://dash.cloudflare.com/43c04c4261b904a7a84467d8950b7b9c/dps.com.au/dns

Step Two:

./init.sh 1.2.3.4

Step Three:

ssh dps@host.wp.helloleaders.com.au
cd linode/wordpress-helloleaders-au
./setup.sh
Script is not completely non-interactive, some confirmations are required.

Step Four:

sudo reboot

Step Five:

Deploy each app
envoy run deploy --uc
Omit --uc if you wish to deploy master branch.
