#!/bin/bash

####################################################
## SETUP.SH - PROVISION THE HOST                  ##
####################################################

set -e
clear

export DEBIAN_FRONTEND=noninteractive

####################################################
## Update the host
####################################################

sudo apt-get update
sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages full-upgrade

####################################################
## Unattended Upgrades
####################################################

sudo apt-get install -y unattended-upgrades update-notifier-common
sudo systemctl enable unattended-upgrades
sudo systemctl restart unattended-upgrades

echo 'Unattended-Upgrade::Mail "cto@dps.com.au";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::MailReport  "on-change";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-WithUsers "true";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot-Time "20:00";' | sudo tee -a /etc/apt/apt.conf.d/50unattended-upgrades

####################################################
## Install generic tools
####################################################

sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages install pv software-properties-common

####################################################
## MySQL Client
####################################################

sudo apt-get install -y mysql-client

####################################################
## Nginx - Installation
####################################################

sudo apt-get install -y nginx certbot python3-certbot-nginx
sudo chmod 775 /var/www
sudo chown -R dps /var/www
sudo chgrp -R web /var/www
sudo chmod g+s /var/www

####################################################
## PHP (v7.2)
###################################################

sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update
sudo apt-get install -y php7.2-fpm php7.2-imagick php7.2-cli php7.2-xml php7.2-curl \
  php7.2-redis php7.2-dom php7.2-json php7.2-mbstring php7.2-zip php7.2-gd \
  php7.2-mysql php7.2-common php7.2-intl php7.2-opcache php7.2-dev php7.2-bcmath \
  php7.2-mcrypt

####################################################
## Nginx Virtual Hosts
###################################################

# Craft - stg-01.craft.dev.dps.com.au

mkdir /var/www/stg-01.craft.dev.dps.com.au /var/www/stg-01.craft.dev.dps.com.au/releases

sudo cp /home/dps/linode/craft-staging/nginx/craft.conf /etc/nginx/sites-available/craft.conf
sudo ln -s /etc/nginx/sites-available/craft.conf /etc/nginx/sites-enabled/craft.conf

####################################################
## Nginx (Complete and HTTPS Certificates)
###################################################

sudo systemctl restart nginx

sudo certbot \
  --nginx \
  --non-interactive \
  --agree-tos \
  --redirect \
  -m cto@dps.com.au \
  -d stg-01.craft.dev.dps.com.au

####################################################
## Iptables - block HTTP/S access
####################################################

## Localhost
sudo iptables -A INPUT -p tcp --dport 22 -s 127.0.0.1 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -s 127.0.0.1 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -s 127.0.0.1 -j ACCEPT

## Allow all Australian IPs as staff could be using this service from home.
cp /home/dps/linode/craft-staging/ips/australian-ip-ranges.txt /home/dps/linode/craft-staging/ips/australian-ip-ranges-port-various.sh
sed -e 's/$/ -p tcp --match multiport --dports 22,80,443/' -i /home/dps/linode/craft-staging/ips/australian-ip-ranges-port-various.sh
sudo chmod +X /home/dps/linode/craft-staging/ips/australian-ip-ranges-port-various.sh
echo "Allowing port 22, 80, 443 access from Australia"
pv /home/dps/linode/craft-staging/ips/australian-ip-ranges-port-various.sh | sudo bash

## Block all other 22/80/443 connections
sudo iptables -A INPUT -p tcp --dport 22 -j DROP
sudo iptables -A INPUT -p tcp --dport 80 -j DROP
sudo iptables -A INPUT -p tcp --dport 443 -j DROP

####################################################
## Environment Variables
####################################################

echo "" | sudo tee -a /etc/profile
echo "set -a" | sudo tee -a /etc/profile
echo "source /home/dps/linode/craft-staging/env/craft" | sudo tee -a /etc/profile
echo "set +a" | sudo tee -a /etc/profile

sudo cp /home/dps/linode/craft-staging/env/env.php /var/www/stg-01.craft.dev.dps.com.au/env.php

####################################################
## Misc
####################################################

# Set hostname
echo "stg-01.craft.dev.dps.com.au" | sudo tee /etc/hostname

####################################################
## Finishing
###################################################

echo "-------------------------------------------"
echo "Finished. The host will now reboot."
echo "-------------------------------------------"

sudo reboot
