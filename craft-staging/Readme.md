Linode Config
- Ubuntu 20.04 LTS
- Region Sydney, AU
- Shared CPU Linode 1 GB ($5/month, $0.0075/hour)
- Label YYYYMMDD-craft-staging
- Root password `o6ffebJ3Y1nRiLQlID`
- Add private IP address

IP
192.46.221.171

Step One:
Update stg-01.craft.dev.dps.com.au A record to VPS IP Address
https://dash.cloudflare.com/43c04c4261b904a7a84467d8950b7b9c/dps.com.au/dns

Step One:
./init.sh 1.2.3.4

Step Two:
```
ssh dps@stg-01.craft.dev.dps.com.au
cd linode/craft-staging
./setup.sh 
```
Script is not completely non-interactive, some confirmations are required.

Step Four:
sudo reboot

Step Five:
Deploy each app
envoy run deploy --uc
Omit --uc if you wish to deploy master branch.

**TO BE ADDED TO THE SETUP FOR CRAFT STAGING**

Got things configured and managed to upload my 9mb test image to staging. Took me a bit to remember craft lives in a separate environment first though!

Here's my notes for the config changes:Changes made to stg-01.craft.dev.dps.com.auIn /etc/php/7.2/fpm/php.ini:

Changed upload_max_filesize = 2M to 16M

& post_max_size = 8M to 16M

To match craft's defaults.In /etc/nginx/nginx.conf:

Added client max body size to the http section

```
http {
client_max_body_size 16M;
```