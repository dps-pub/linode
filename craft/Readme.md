Linode Config
- Ubuntu 20.04 LTS
- Region Sydney, AU
- Shared CPU Linode 1 GB ($5/month, $0.0075/hour)
- Label YYYYMMDD-craft
- Root password `**63a2K9IXzHCxY2nIS**`

IP
192.46.221.171

Step One:
Update craft.agedcareguide.com.au A record to VPS IP Address
https://dash.cloudflare.com/43c04c4261b904a7a84467d8950b7b9c/agedcareguide.com.au/dns

Step One:
./init.sh 1.2.3.4

Step Two:
```
ssh dps@craft.agedcareguide.com.au
cd linode/craft
./setup.sh 
```
Script is not completely non-interactive, some confirmations are required.

Step Four:
sudo reboot

Step Five:
Deploy each app
envoy run deploy --uc
Omit --uc if you wish to deploy master branch.

